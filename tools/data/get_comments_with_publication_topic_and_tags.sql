/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [Id]
      ,[CrawlDate]
      ,[Url]
      ,[UserUrl]
      ,[CommentDate]
      ,[NumberInPub]
      ,[MoodString]
      ,[VotesUpCount]
      ,[VotesDownCount]
      ,[BodyText]
      ,[ContentHtml]
      ,[Username]
      ,[UserRating]
      ,[IsDeleted]
      ,[HasErrors]
	  ,(SELECT Name + ', ' AS 'data()' 
FROM [Tags]
Where Id in (select Tag_Id from TagPublications tp where tp.Publication_Id = c.Publication_Id)
FOR XML PATH('')) as Tags
	  ,(SELECT Name + ', ' AS 'data()' 
FROM [Topics]
Where Id in (select Topic_Id from TopicPublications t where t.Publication_Id = c.Publication_Id)
FOR XML PATH('')) as Topics
  FROM [Comments] c